#include <stdio.h>
#include <stdlib.h>
#include "../inc/list.h"

/**
 * List Operations
 */

//initialises list, allocating it a value of NULL
LeafList *initList() {
  LeafList *newList = NULL;
  return newList;
}

//adds leaf node to leaf list
void addLeafToList(LeafList *list, Node *node) {
  node->nextLeaf = list->topLeaf;
  list->topLeaf = node;
  return;
}

// recursively traverses tree, adding leaf nodes to leaf list
void fillList(LeafList *list, Node *head) {
  int i;
  if(head->child[0] == NULL){
    addLeafToList(list, head);
  } else {
    for(i = 0; i < 4; ++i) {
      fillList(list, head->child[i]);
    }
  }
  return;
}

// fills a list and returns it using quadtree
void createList(Quadtree *tree) {
  // memory allocated for a new list
  LeafList *newList = (LeafList *)malloc(sizeof(LeafList));
  newList->topLeaf = NULL;
  // list is filled with child nodes
  fillList(newList, tree->head);
  //frees a previously defined list
  free(tree->list);
  tree->list = newList;
  return;
}
