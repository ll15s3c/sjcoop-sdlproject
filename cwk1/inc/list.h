#ifndef LIST_H
#define LIST_H
#include "treeNode.h"

//initialises list, allocating memory for its use
LeafList *initList();
//adds a given node to the list
void addLeafToList(LeafList *list, Node *node);
//fills list with data (list of child nodes)
void fillList(LeafList *list, Node *head);
//creates new list and calls fillList()
void createList(Quadtree *tree);


#endif
