/*# # # # # # # # # # # # # # # #
  # Painting Program - COMP1921 #
  #   by Sam Cooper - ll15s3    #
  #       Date: 27/04/17        #
  # # # # # # # # # # # # # # # #*/

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "../inc/SDL_Structures.h"
#include "../inc/SDL_UI.h"

/* displays a chosen message on-screen next to buttons */
void displayMessage(char* message) {
  // set font colour
  SDL_Color fontColour = {0, 0, 0};
  // create surface with font
  SDL_Surface* sMessage = TTF_RenderText_Solid(font, message, fontColour);
  // attain width and height of text
  int w = sMessage->w;
  int h = sMessage->h;
  // create texture and free surface as not needed
  SDL_Texture* tMessage = SDL_CreateTextureFromSurface(renderer, sMessage);
  SDL_FreeSurface(sMessage);
  // rectangle and render to clear previous text
  SDL_Rect rect = {800, SCREEN_HEIGHT + 20, 150, h};
  SDL_SetRenderDrawColor(renderer, 125, 125, 125, 0);
  SDL_RenderFillRect(renderer, &rect);
  // correct rectangle width and render message
  rect.w = w;
  SDL_RenderCopy(renderer, tMessage, NULL, &rect);
  // destroy texture as no longer needed
  SDL_DestroyTexture(tMessage);
  return;
}


/* loads provided spritesheet as a texture */
void initPalette() {
  // load image to surface -> load surface to texture
  tPaletteSprites = IMG_Load("assets/colourSprites.png");
  paletteSprites = SDL_CreateTextureFromSurface(renderer, tPaletteSprites);
  // surface no longer needed
  SDL_FreeSurface(tPaletteSprites);
  tPaletteSprites = NULL;
  return;
}


/* renders spritesheet accordingly, spread at bottom of window */
void displayPalette() {
  // variables to hold x and y positions
  int srcX = 0;
  int srcY = 0;
  int destX = 20;
  // variables to control the length of loops
  int outerLoop = ((NUM_BUTTONS - (NUM_BUTTONS % 5)) / 5) + 1; // operations decide the number of rows
  int innerLoop = 5;

  // setting the background colour of the palette
  SDL_Rect bg = {0, SCREEN_HEIGHT, SCREEN_WIDTH, 80};
  SDL_SetRenderDrawColor(renderer, 125, 125, 125, 0);
  SDL_RenderFillRect(renderer, &bg);

  // rendering the buttons at the bottom of the screen, with gaps of 10px
  for(int j = 0; j < outerLoop; j++) {
    if(j == outerLoop - 1) { // if loop is at end row
      // decide how many buttons to display for last row on sprite sheet
      innerLoop = ((NUM_BUTTONS % 5) == 0) ? 5 : NUM_BUTTONS % 5;
    }
    for(int i = 0; i < innerLoop; i++) {
      // render sprites
      SDL_Rect srcRec = {srcX, srcY, 40, 40};
      SDL_Rect destRec = {destX, SCREEN_HEIGHT + 20, 40, 40};
      SDL_RenderCopy(renderer, paletteSprites, &srcRec, &destRec);
      // increase x positions for next iteration
      srcX += 40;
      destX += 50;
    }
    // reset x upon reaching end of row, change y for next row
    srcX = 0;
    srcY += 40;
  }
  return;
}
