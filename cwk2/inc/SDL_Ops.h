/*# # # # # # # # # # # # # # # #
  # Painting Program - COMP1921 #
  #   by Sam Cooper - ll15s3    #
  #       Date: 27/04/17        #
  # # # # # # # # # # # # # # # #*/

#ifndef SDLOPS_H
#define SDLOPS_H

#include <stdbool.h>

// initialise SDL structures (window, renderer, canvas) returns false if failure
bool init();
// returns screen height (for modules without access)
int getScreenHeight();
// returns true if left mouse button is held down
bool leftMouseDown();
// destroys SDL structures and resets memory
void closeSDL();

#endif
